<?php

function og_domain_admin_page() {
  $out = '';
  
  // set some internal variables
  $out .= drupal_get_form('og_domain_admin_page_form');

  return $out;
}

function og_domain_admin_page_form() {
  $form = array(
    'og_domain_default_value' => array(
      '#type' => 'textfield',
      '#title' => 'Default domain',
      '#default_value' => variable_get('og_domain_default_value', _og_domain_get_base_url_domain()),
    ),
    'og_domain_disabled_page' => array(
      '#type' => 'textfield',
      '#title' => 'Redirect page when the domain is disabled',
      '#default_value' => variable_get('og_domain_disabled_page', _og_domain_get_default_disabled_page()),
    ),
  );
  return system_settings_form(array_merge_recursive($form, _og_domain_invoke_all('admin_form')));
}

function _og_domain_check_mappings_access($record) {
  return node_access("update", $record['node']);
}

function og_domain_mappings_admin_page($all = FALSE) {
  $header = array('Group', 'Domain', '&nbsp;');
  $rows = array();
  $records = _og_domain_get_all(OG_DOMAIN_WITH_NODEOBJECTS);
  if(!$all) $records = array_filter($records, '_og_domain_check_mappings_access');
  foreach($records as $r) {
    $rows []= array(
      l($r['node']->title, 'node/'.$r['node']->nid) . ' (' . l(t('Edit'), 'node/'.$r['node']->nid.'/edit') . ')',
      $r['domain'],
      l(t('Unmap'), 'admin/og/domain/mappings/unmap/'.$r['node']->nid),
    );
  }
  return theme_table($header, $rows);
}

function og_domain_mappings_unmap_admin_page($nid) {
  if(_og_domain_db_lookup_domain($nid) === FALSE) drupal_goto('admin/og/domain/mappings');
  else return drupal_get_form('og_domain_unmap_form', $nid);
}

function og_domain_unmap_form($form_object, $nid) {
  $form = array();
  
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['disclaimer'] = array(
    '#type' => 'item',
    '#title' => 'Are you sure you want to delete this mapping?',
    '#description' => t('This action cannot be undone'),
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  return $form;
}

function og_domain_unmap_form_validate($form, &$form_state) {
  
}

function og_domain_unmap_form_submit($form, &$form_state) {
  _og_domain_unmap_by_nid($form_state['values']['nid']);
}